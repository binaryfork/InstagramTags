package com.bf.instagramtags.ui;

import android.util.Log;

import com.bf.instagramtags.api.InstagramApi;
import com.bf.instagramtags.api.entities.Media;
import com.bf.instagramtags.api.entities.MediaList;

import java.util.ArrayList;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainPresenter {

    private MainView view;
    private Subscription subscription;

    public void loadMediaByTag(String query) {
        if (subscription != null)
            subscription.unsubscribe();
        subscription = InstagramApi.service().taggedMediaList(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<MediaList>() {
                    @Override public void onCompleted() {
                    }

                    @Override public void onError(Throwable e) {
                        Log.e("MainPresenter", "loadMediaByTag ", e);
                        view.onError(e.getMessage());
                    }

                    @Override public void onNext(MediaList mediaList) {
                        view.onLoaded(mediaList.data);
                    }
                });
    }

    public void detach() {
        if (subscription != null)
            subscription.unsubscribe();
    }

    public void setView(MainView view) {
        this.view = view;
    }

    interface MainView {
        void onLoaded(ArrayList<Media> media);

        void onError(String message);
    }
}
