package com.bf.instagramtags.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bf.instagramtags.R;
import com.bf.instagramtags.api.entities.Media;
import com.squareup.picasso.Picasso;

public class MediaVH extends RecyclerView.ViewHolder {

    private final ImageView userPic;
    private final ImageView image;
    private final TextView userName;
    private final TextView date;

    public MediaVH(View itemView) {
        super(itemView);
        userPic = (ImageView) itemView.findViewById(R.id.userPic);
        userName = (TextView) itemView.findViewById(R.id.userName);
        date = (TextView) itemView.findViewById(R.id.date);
        image = (ImageView) itemView.findViewById(R.id.image);
    }

    public void bind(final Media media) {
        userName.setText(media.user.username);
        date.setText(DateUtils.getRelativeTimeSpanString(media.created_time * 1000));
        Picasso.with(itemView.getContext()).load(media.images.standard_resolution.url).into(image);
        Picasso.with(itemView.getContext()).load(media.user.profile_picture).into(userPic);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(media.link));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                itemView.getContext().startActivity(intent);
            }
        });
    }
}
