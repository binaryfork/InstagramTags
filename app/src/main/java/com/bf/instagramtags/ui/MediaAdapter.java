package com.bf.instagramtags.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bf.instagramtags.R;
import com.bf.instagramtags.api.entities.Media;

import java.util.ArrayList;

public class MediaAdapter extends RecyclerView.Adapter<MediaVH> {

    private final LayoutInflater inflater;
    private ArrayList<Media> data;

    public MediaAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    @Override public MediaVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MediaVH(inflater.inflate(R.layout.instagram_media_vh, parent, false));
    }

    @Override public void onBindViewHolder(MediaVH holder, int position) {
        holder.bind(data.get(position));
    }

    @Override public int getItemCount() {
        if (data == null)
            return 0;
        else
            return data.size();
    }

    public void setData(ArrayList<Media> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void clearData() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }
}
