package com.bf.instagramtags.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bf.instagramtags.BaseApp;
import com.bf.instagramtags.R;
import com.bf.instagramtags.api.entities.Media;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements MainPresenter.MainView {

    private RecyclerView recyclerView;
    private View progressBar;
    private MediaAdapter mediaAdapter;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        mediaAdapter = new MediaAdapter(getBaseContext());
        recyclerView.setAdapter(mediaAdapter);

        BaseApp.mainPresenter.setView(this);
    }

    @Override public void onLoaded(ArrayList<Media> media) {
        progressBar.setVisibility(View.GONE);
        if (media.size() == 0) {
            onError(getString(R.string.error_photos_not_foung));
            return;
        }
        mediaAdapter.setData(media);
    }

    @Override public void onError(String message) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override protected void onDestroy() {
        BaseApp.mainPresenter.detach();
        super.onDestroy();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setQueryHint("Search");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override public boolean onQueryTextChange(String s) {
                if (s.length() > 0) {
                    progressBar.setVisibility(View.VISIBLE);
                    mediaAdapter.clearData();
                    BaseApp.mainPresenter.loadMediaByTag(s);
                }
                return false;
            }
        });
        return true;
    }
}
