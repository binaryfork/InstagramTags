package com.bf.instagramtags.api;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class InstagramApi {

    public static final String ENDPOINT = "https://api.instagram.com/v1/";
    private static Retrofit restAdapter;

    public static void setup() {
        restAdapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    public static InstagramService service() {
        if (restAdapter == null)
            setup();
        return restAdapter.create(InstagramService.class);
    }

    private static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
        okClientBuilder.addInterceptor(paramInterceptor);
        return okClientBuilder.build();
    }

    private static final Interceptor paramInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            HttpUrl url = request.url().newBuilder()
                    .addQueryParameter("client_id", "a4de6e3e2730466f9db7c6d9b17a1a7b")
                    .build();
            request = request.newBuilder().url(url).build();
            return chain.proceed(request);
        }
    };
}
