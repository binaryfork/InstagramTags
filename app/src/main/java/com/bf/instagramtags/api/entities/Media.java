package com.bf.instagramtags.api.entities;

public class Media {
    public String link;
    public long created_time;
    public MediaImages images;
    public User user;
}
