package com.bf.instagramtags.api;

import com.bf.instagramtags.api.entities.MediaList;


import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface InstagramService {
    @GET("tags/{tag}/media/recent") Observable<MediaList> taggedMediaList(@Path("tag") String tag);
}
