package com.bf.instagramtags;

import android.app.Application;

import com.bf.instagramtags.ui.MainPresenter;

public class BaseApp extends Application {

    public static MainPresenter mainPresenter;

    @Override public void onCreate() {
        super.onCreate();
        mainPresenter = new MainPresenter();
    }
}
